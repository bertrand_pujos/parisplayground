/**
 * Module dependencies.
 */
require('newrelic')
var express = require('express')
var compress = require('compression')
// var morgan = require('morgan')
var http = require('http')
var path = require('path')

var errorhandler = require('errorhandler')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')
var oneYear = 31557600000
var app = express()
var api = require('./controller/api.js')
var logger = require('./util/logger.js')

// all environments
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, '/views'))
app.set('view engine', 'haml')
app.use(compress())

app.use(express.static(path.join(__dirname, 'client'), { maxAge: oneYear }))

// development only
logger.debug('Overriding Express logger')
logger.info(app.get('env'))
app.use(require('morgan')('combined', { 'stream': logger.stream }))
if (app.get('env') === 'development') {
  app.use(errorhandler())
}

app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())
app.use(methodOverride())
app.get('/', api.index)
app.get('/playgrounds', api.neargames)
app.get('/ratpwithin', api.ratpwithin)
app.get('/searchgames', api.searchgames)
app.get('/playgroundfullcalendar', api.fullcalendar)

var engines = require('consolidate')
app.engine('haml', engines.haml)

http.createServer(app).listen(app.get('port'), function () {
  logger.info('Express server listening on port ' + app.get('port'))
})

exports.app = app
