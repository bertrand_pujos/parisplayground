var winston = require('winston')
//  var Logentries = require('winston-logentries')
require('winston-logentries')

winston.emitErrs = true

var logger
if (process.env.NODE_ENV === 'production') {
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Logentries)({ token: '6add8e98-5f01-4d52-9e43-925828356ff8', level: 'info' })
    ],
    exitOnError: false
  })
  logger.info('Just setup logentries for production')
} else if (process.env.NODE_ENV === 'test') {
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Logentries)({ colorize: true, token: 'f22296a7-688f-4984-908c-384bcfe7df66', level: 'debug' })
    ],
    exitOnError: false
  })
  logger.info('Just setup logentries for test')
} else {
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true }),
      new (winston.transports.Logentries)({ colorize: true, token: 'f22296a7-688f-4984-908c-384bcfe7df66', level: 'debug' })
    ],
    exitOnError: false
  })
  logger.info('Just setup logentries for other environment')
}

module.exports = logger
module.exports.stream = {
  write: function (message, encoding) {
    logger.info(message.slice(0, -1))
  }
}
