var chai = require('chai')
//  expect = chai.expect()
var rewire = require('rewire')
var should = chai.should()
var request = require('supertest')

var appli = rewire('../controller/api.js')

var app = require('../server')

var logger = require('../util/logger.js')

var gamesWithQuery = appli.__get__('gamesWithQuery')
var makeQuery = appli.__get__('makeQuery')
var ratpWithQuery = appli.__get__('ratpWithQuery')

describe('Playground Query', function () {
  describe('Unit Tests', function () {
    describe('RATP Search - unitTests', function () {
      describe('ratpWithQuery', function () {
        var feat

        before(function (done) {
          this.timeout(25000)
          var query = {}

          query = {geometry: { $geoWithin: {$geometry: {type: 'Polygon',
            coordinates: [[[2.35652817, 48.86265770],
                    [2.35652817, 48.86625459],
                    [2.36494004, 48.86625459],
                    [2.36494004, 48.86265770],
            [2.35652817, 48.86265770]]]}}}}
          ratpWithQuery(query,
            function (err, docs) {
              if (err) { return [] }
              feat = docs
              logger.info(JSON.stringify(feat))
              done()
            })
        })

        it('should find 5 ratp stations', function () {
          feat.features.length.should.equal(5)
        })

        it('the first feature properties STATION_NOM should be ARTS ET METIERS', function () {
          feat.features[0].properties.STATION_NOM.should.equal('ARTS ET METIERS')
        })
      })
    })

    describe('Playground Search - unitTests', function () {
      describe('makeQuery', function () {
        it('should have a geometry property', function () {
          makeQuery(48.82338823, 2.28924685, 48.83864126, 2.30529235, 2, '', 0, 0, null).should.have.property('geometry')
        })

        it("shouldn't have a geometry property", function () {
          makeQuery(0, 0, 0, 0, 2, '', 0, 0, null).should.not.have.property('geometry')
        })

        it("shouldn't have a TYPE property", function () {
          should.not.exist(makeQuery(0, 0, 0, 0, 2, '', 0, 0, null)['properties.TYPE'])
        })
      })

      describe('gamesWithQuery', function () {
        var feat

        before(function (done) {
          this.timeout(25000)
          var query = {}
          query['properties.GLOBALMIN'] = {$lte: 2}
          query['properties.GLOBALMAX'] = {$gte: 2}

          query.geometry = { $geoWithin: {$geometry: {type: 'Polygon',
            coordinates: [[[2.28924685, 48.82338823],
                  [2.28924685, 48.83864126],
                  [2.3052923, 48.83864126],
                  [2.3052923, 48.82338823],
                [2.28924685, 48.82338823]]]}
          }
          }
          gamesWithQuery(query,
            function (err, docs) {
              if (err) { return [] }
              feat = docs
              logger.info(JSON.stringify(feat))
              done()
            })
        })

        it('should find 10 playgrounds', function () {
          feat.features.length.should.equal(10)
        })

        it('the first feature properties ARR should be XV', function () {
          feat.features[0].properties.ARR.should.equal(15)
        })
      })
    })
  })

  describe('Integration Tests', function () {
    it('should return playgrounds', function (done) {
      this.timeout(25000)
      request(app.app)
      .get('/playgrounds?swlat=48.82338823&swlng=2.28924685&nelat=48.83864126&nelng=2.30529235&age=2')
      .expect('Content-type', 'text/html; charset=utf-8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err)
          // HTTP status should be 200
        res.status.should.equal(200)
        done()
      })
    })

    it('should return calendar', function (done) {
      this.timeout(25000)
      request(app.app)
        .get('/playgroundfullcalendar?equipment=http://equipement.paris.fr/square-jules-ferry-2588')
        .expect('Content-type', 'text/html; charset=utf-8')
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err)
          // HTTP status should be 200
          res.status.should.equal(200)
          done()
        })
    })

    it('should return one playground (html)', function (done) {
      this.timeout(25000)
      request(app.app)
        .get('/searchgames?arrondissement=4&bac=1')
        .expect('Content-type', 'text/html; charset=utf-8')
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err)
          // HTTP status should be 200
          res.status.should.equal(200)
          done()
        })
    })

    it('should return one playground (json)', function (done) {
      this.timeout(25000)
      request(app.app)
        .get('/searchgames?arrondissement=4&bac=1&json=1')
        .expect('Content-type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err)
          // HTTP status should be 200
          res.status.should.equal(200)
          done()
        })
    })
  })
})
