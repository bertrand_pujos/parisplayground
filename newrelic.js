/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
var logger = require('./util/logger.js')
if (process.env.NODE_ENV === 'production') {
  logger.info('Setting newrelic for production')
  exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['production', 'Paris Playground API'],
    /**
     * Your New Relic license key.
     */
    license_key: '6fa64f362181713dccdf61ee73296519ca4d40b3',
    logging: {
      /**
       * Level at which to log. 'trace' is most useful to New Relic when diagnosing
       * issues with the agent, 'info' and higher will impose the least overhead on
       * production applications.
       */
      level: 'info'
    }
  }
} else {
  logger.info('Setting newrelic for development')
  exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['development', 'Paris Playground API'],
    /**
     * Your New Relic license key.
     */
    license_key: '6fa64f362181713dccdf61ee73296519ca4d40b3',
    logging: {
      /**
       * Level at which to log. 'trace' is most useful to New Relic when diagnosing
       * issues with the agent, 'info' and higher will impose the least overhead on
       * production applications.
       */
      level: 'trace'
    }
  }
}
