/*
 * Copyright (c) 2012., Qualcomm, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License")
 *    you may not use this file except in compliance with the License.
 *
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//  MONGOHQ
//  var dbUrlMongojs = "selvatech:bartsimp@paulo.mongohq.com:10094/SelvatechPlayground"
//  MONGOLAB
//  var dbUrlMongojs = "selvatech2:bartsimp@ds045099.mongolab.com:45099/selvatechplayground"
var dbUrlMongojs = 'selvatech2:bartsimp@ds059509.mongolab.com:59509/parisplayground'
var collectionsMongojs = ['PlaygroundLocation', 'RATPStop']
var mongojs = require('mongojs')
var dbMongojs = mongojs(dbUrlMongojs, collectionsMongojs, {authMechanism: 'ScramSHA1'})
//  var p = require('ua-parser')
var yql = require('yqlp')
var logger = require('../util/logger.js')

exports.index = function (req, res) {
  res.render('index', {
  })
}

// exports.calendar = function (req, res) {
//   var equipment = ''
//   if (req.query.equipment) {
//     equipment = req.query.equipment
//   }
//   logger.debug(equipment)

//   if (equipment !== '') {
//     var query = "select content from html where url='" + equipment + "'"
//     query = query + ' and xpath=\'//div[@id="local-calendar"]/p\' '
//     // logger.debug(query)
//     yql.exec(query, function (error, response) {
//       if (error) {
//         if (req.query.json) {
//           res.json(JSON.stringify('n/a'))
//         } else {
//           res.send(JSON.stringify('n/a'))
//         }
//         //  res.send(JSON.stringify('n/a'))
//         //  res.json(JSON.stringify('n/a'))
//       }
//       var paras = response.query.results.p
//       if (paras.length === 0) {
//         //  res.send(JSON.stringify('n/a'))
//         if (req.query.json) {
//           res.json(JSON.stringify('n/a'))
//         } else {
//           res.send(JSON.stringify('n/a'))
//         }
//       }
//       logger.debug(paras[0])
//       var firstperiod = paras[0].split(' ')
//       var now = new Date()
//       var stDate = now.getDay()
//       if (firstperiod[0] === 'Du') {
//         if (stDate === 0) {
//           //  res.send(JSON.stringify(paras[8]))
//           //  res.json(JSON.stringify(paras[8]))
//           if (req.query.json) {
//             res.json(JSON.stringify(paras[8]))
//           } else {
//             res.send(JSON.stringify(paras[8]))
//           }
//         } else {
//           //  res.send(JSON.stringify(paras[stDate]))
//           //  res.json(JSON.stringify(paras[stDate]))
//           if (req.query.json) {
//             res.json(JSON.stringify(paras[stDate]))
//           } else {
//             res.send(JSON.stringify(paras[stDate]))
//           }
//         }
//       } else {
//         //  res.send(JSON.stringify('n/a'))
//         if (req.query.json) {
//           res.json(JSON.stringify('n/a'))
//         } else {
//           res.send(JSON.stringify('n/a'))
//         }
//       }
//     })
//   } else {
//     //  res.send(JSON.stringify('n/a'))
//     if (req.query.json) {
//       res.json(JSON.stringify('n/a'))
//     } else {
//       res.send(JSON.stringify('n/a'))
//     }
//   }
// }

exports.fullcalendar = function (req, res) {
  var equipment = ''
  if (req.query.equipment) {
    equipment = req.query.equipment
  }
  logger.info(equipment)
  if (equipment !== '') {
    var query = "select content from html where url='" + equipment + "'"
    query = query + " and xpath='//div[@id=\"local-calendar\"]/p' "
    yql.exec(query, function (error, response) {
      if (error) {
        if (req.query.json) {
          res.json(JSON.stringify('n/a'))
        } else {
          res.send(JSON.stringify('n/a'))
        }
      }
      if (response.query.results != null) {
        var paras = response.query.results.p
        if (paras.length === 0) {
          if (req.query.json) {
            res.json(JSON.stringify('n/a'))
          } else {
            res.send(JSON.stringify('n/a'))
          }
        } else {
          logger.info(paras)
          var hourCollection = new HourCollection()
          var t, j, k, m, n
          var entete = []
          var entetec = 0
          for (j = 0; j < paras.length; j++) {
            var l = paras[j].trim().substr(0, 3).toUpperCase()
            if (l !== 'LUN' && l !== 'MAR' && l !== 'MER' && l !== 'JEU' && l !== 'VEN' && l !== 'SAM' && l !== 'DIM') {
              entete[entetec] = j
              entetec = entetec + 1
            }
          }

          for (k = 0; k < entete.length; k++) {
            t = new Hours()
            t.NAME = paras[entete[k]]
            n = 0
            if (entete.length === k + 1) {
              for (m = entete[k] + 1; m < paras.length; m++) {
                t.HOUR[n] = paras[m].replace(/(\r\n|\n|\r)/gm, ' ').replace(/\s+/g, ' ')
                n = n + 1
              }
            } else {
              for (m = entete[k] + 1; m < entete[k + 1]; m++) {
                t.HOUR[n] = paras[m].replace(/(\r\n|\n|\r)/gm, ' ').replace(/\s+/g, ' ')
                n = n + 1
              }
            }
            hourCollection.periods[k] = t
          }
          if (req.query.json) {
            res.json(JSON.stringify(hourCollection))
          } else {
            res.send(JSON.stringify(hourCollection))
          }
        }
      } else {
        if (req.query.json) {
          res.json(JSON.stringify('n/a'))
        } else {
          res.send(JSON.stringify('n/a'))
        }
      }
    })
  } else {
    if (req.query.json) {
      res.json(JSON.stringify('n/a'))
    } else {
      res.send(JSON.stringify('n/a'))
    }
  }
}

exports.ratpwithin = function (req, res) {
  var swlat = Number(req.query.swlat)
  var swlng = Number(req.query.swlng)
  var nelat = Number(req.query.nelat)
  var nelng = Number(req.query.nelng)
  //  var featureCollection = new FeatureCollection()
  //  var i = 0

  var query = {geometry: { $geoWithin: {$geometry: {type: 'Polygon',
    coordinates: [[[swlng, swlat],
            [swlng, nelat],
            [nelng, nelat],
            [nelng, swlat],
    [swlng, swlat]]]}}}}

  ratpWithQuery(query,
    function (err, docs) {
      if (err) { return [] }
      //  res.send(JSON.stringify(docs))
      //  res.json(JSON.stringify(docs))
      if (req.query.json) {
        res.json(JSON.stringify(docs))
      } else {
        res.send(JSON.stringify(docs))
      }
    })
}

function ratpWithQuery (query, callback) {
  var i = 0
  var featureCollection = new FeatureCollection()

  dbMongojs.RATPStop.find(query,
    function (err, reply) {
      if (err) { return [] }
      // return docs
      reply.forEach(function (store) {
        var pl = new RATPL()
        var pr = new RATPStopProperties()
        pr.STATION_NOM = store.properties.STATION_NOM
        pr.STATION_ID = store.properties.STATION_ID
        pr.LIGNES = store.properties.LIGNES
        pr.STATION_NETWORK = store.properties.STATION_NETWORK
        pr.STATION_CITY = store.properties.STATION_CITY
        // pl.distance=store.dis
        pl.geometry = store.geometry
        pl._id = store._id

        pl.properties = pr
        featureCollection.features[i] = pl

        var ligne = ''
        var j = 0
        for (j = 0; j < store.properties.LIGNES.length; j++) { ligne += JSON.stringify(store.properties.LIGNES[j].LIGNE_CODE) + ' ' }
        logger.info(i + ': ' + JSON.stringify(store.properties.STATION_ID) + ' : ' + JSON.stringify(store.properties.STATION_NOM) + ' ' + ligne)
        i = i + 1
      })
      return callback(null, featureCollection)
    }
  )
}

// exports.search = function (req, res) {
//   var age = 0
//   if (req.query.age) {
//     age = Number(req.query.age)
//   }
//   var arr = 0
//   if (req.query.arrondissement) {
//     arr = Number(req.query.arrondissement)
//   }
//   var bal = 0
//   if (req.query.bal) {
//     bal = Number(req.query.bal)
//   }
//   var bac = 0
//   if (req.query.bac) {
//     bac = Number(req.query.bac)
//   }
//   var spo = 0
//   if (req.query.spo) {
//     spo = Number(req.query.spo)
//   }
//   var str = 0
//   if (req.query.str) {
//     str = Number(req.query.str)
//   }
//   var cab = 0
//   if (req.query.cab) {
//     cab = Number(req.query.cab)
//   }
//   var tou = 0
//   if (req.query.tou) {
//     tou = Number(req.query.tou)
//   }
//   var gri = 0
//   if (req.query.gri) {
//     gri = Number(req.query.gri)
//   }
//   var reso = 0
//   if (req.query.reso) {
//     reso = Number(req.query.reso)
//   }
//   var aut = 0
//   if (req.query.aut) {
//     aut = Number(req.query.aut)
//   }
//   var tob = 0
//   if (req.query.tob) {
//     tob = Number(req.query.tob)
//   }

//   // var j=0
//   // var featureCollection = new FeatureCollection()
//   var reqgame = bal + bac + spo + str + cab + tou + gri + reso + aut + tob
//   var gamearray = []
//   var i = 0
//   if (reqgame > 0) {
//     if (bal > 0) {
//       gamearray[i] = 'balançoire'
//       i++
//     }
//     if (bac > 0) {
//       gamearray[i] = 'bac à sable'
//       i++
//     }
//     if (spo > 0) {
//       gamearray[i] = 'sport'
//       i++
//     }
//     if (str > 0) {
//       gamearray[i] = 'structure'
//       i++
//     }
//     if (cab > 0) {
//       gamearray[i] = 'cabane'
//       i++
//     }
//     if (tou > 0) {
//       gamearray[i] = 'tourniquet'
//       i++
//     }
//     if (gri > 0) {
//       gamearray[i] = 'jeu à grimper'
//       i++
//     }
//     if (reso > 0) {
//       gamearray[i] = 'jeu à ressort'
//       i++
//     }
//     if (aut > 0) {
//       gamearray[i] = 'autre'
//       i++
//     }
//     if (tob > 0) {
//       gamearray[i] = 'toboggan'
//       i++
//     }
//   }

//   var query = makeQuery(0, 0, 0, 0, age, 'FREE_GAME', arr, reqgame, gamearray)
//   gamesWithQuery(query,
//     function (err, docs) {
//       if (err) { return [] }
//       //  res.send(JSON.stringify(docs))
//       //  res.json(JSON.stringify(docs))
//       if (req.query.json) {
//         res.json(JSON.stringify(docs))
//       } else {
//         res.send(JSON.stringify(docs))
//       }
//     })
// }

exports.searchgames = function (req, res) {
  var age = 0
  if (req.query.age) {
    age = Number(req.query.age)
  }
  var arr = 0
  if (req.query.arrondissement) {
    arr = Number(req.query.arrondissement)
  }
  var bal = 0
  if (req.query.bal) {
    bal = Number(req.query.bal)
  }
  var bac = 0
  if (req.query.bac) {
    bac = Number(req.query.bac)
  }
  var spo = 0
  if (req.query.spo) {
    spo = Number(req.query.spo)
  }
  var str = 0
  if (req.query.str) {
    str = Number(req.query.str)
  }
  var cab = 0
  if (req.query.cab) {
    cab = Number(req.query.cab)
  }
  var tou = 0
  if (req.query.tou) {
    tou = Number(req.query.tou)
  }
  var gri = 0
  if (req.query.gri) {
    gri = Number(req.query.gri)
  }
  var reso = 0
  if (req.query.reso) {
    reso = Number(req.query.reso)
  }
  var aut = 0
  if (req.query.aut) {
    aut = Number(req.query.aut)
  }
  var tob = 0
  if (req.query.tob) {
    tob = Number(req.query.tob)
  }
  var man = 0
  if (req.query.man) {
    man = Number(req.query.man)
  }

  var reqgame = bal + bac + spo + str + cab + tou + gri + reso + aut + tob + man
  var gamearray = []
  var i = 0
  if (reqgame > 0) {
    if (bal > 0) {
      gamearray[i] = 'balançoire'
      i++
    }
    if (bac > 0) {
      gamearray[i] = 'bac à sable'
      i++
    }
    if (spo > 0) {
      gamearray[i] = 'sport'
      i++
    }
    if (str > 0) {
      gamearray[i] = 'structure'
      i++
    }
    if (cab > 0) {
      gamearray[i] = 'cabane'
      i++
    }
    if (tou > 0) {
      gamearray[i] = 'tourniquet'
      i++
    }
    if (gri > 0) {
      gamearray[i] = 'jeu à grimper'
      i++
    }
    if (reso > 0) {
      gamearray[i] = 'jeu à ressort'
      i++
    }
    if (aut > 0) {
      gamearray[i] = 'autre'
      i++
    }
    if (tob > 0) {
      gamearray[i] = 'toboggan'
      i++
    }
    if (man > 0) {
      gamearray[i] = 'manège'
      i++
    }
  }

  var query = makeQuery(0, 0, 0, 0, age, '', arr, reqgame, gamearray)
  gamesWithQuery(query,
    function (err, docs) {
      if (err) { return [] }
      if (req.query.json) {
        res.json(JSON.stringify(docs))
      } else {
        res.send(JSON.stringify(docs))
      }
    })
}

/*  exports.near2 = function (req, res) {
  var swlat = Number(req.query.swlat)
  var swlng = Number(req.query.swlng)
  var nelat = Number(req.query.nelat)
  var nelng = Number(req.query.nelng)
  var kidage = 0
  if (req.query.age) {
    kidage = Number(req.query.age)
  }

  var query = makeQuery(swlat, swlng, nelat, nelng, kidage, 'FREE_GAME', 0, 0, null)
  gamesWithQuery(query,
    function (err, docs) {
      if (err) { return [] }
      if (req.query.json) {
        res.json(JSON.stringify(docs))
      } else {
        res.send(JSON.stringify(docs))
      }
    })
} */

function makeQuery (swlat, swlng, nelat, nelng, kidage, type, arr, reqgame, gamearray) {
  var query = {}
  if (kidage > 0) {
    query['properties.GLOBALMIN'] = {$lte: kidage}
    query['properties.GLOBALMAX'] = {$gte: kidage}
  }
  if (swlat > 0 && swlng > 0 && nelat > 0 && nelng > 0) {
    query.geometry = { $geoWithin: {$geometry: {type: 'Polygon',
      coordinates: [[[swlng, swlat],
            [swlng, nelat],
            [nelng, nelat],
            [nelng, swlat],
          [swlng, swlat]]]}
    }
    }
  }
  if (type !== '') {
    query['properties.TYPE'] = type
  }
  if (arr > 0) {
    query['properties.ARR'] = arr
  }
  if (reqgame > 0) {
    query['properties.GAMES.GAME_TYPE'] = {$in: gamearray}
  }
  logger.debug('Query generated : ' + JSON.stringify(query))
  return query
}

function gamesWithQuery (query, callback) {
  var i = 0
  var featureCollection = new FeatureCollection()

  dbMongojs.PlaygroundLocation.find(query,
    {'properties.GLOBALMAX': 1,
      'properties.GLOBALMIN': 1,
      'properties.GAMES': 1,
      'properties.NOM': 1,
      'properties.DENOM': 1,
      'properties.ACCES': 1,
      'properties.HORAIRE': 1,
      'properties.ARR': 1,
      'properties.URL': 1,
      'properties.TYPE': 1,
      'properties.ADDR': 1,
      geometry: 1},
    function (err, docs) {
      if (err) { return [] }
      // return docs
      docs.forEach(function (store) {
        var pl = new PlaygroundL()
        var pr = new PlaygroundProperties()
        pr.GLOBALMAX = store.properties.GLOBALMAX
        pr.GLOBALMIN = store.properties.GLOBALMIN
        pr.GAMES = store.properties.GAMES
        pr.NOM = store.properties.NOM
        pr.DENOM = store.properties.DENOM
        pr.ACCES = store.properties.ACCES
        pr.HORAIRE = store.properties.HORAIRE
        pr.URL = store.properties.URL
        pr.TYPE = store.properties.TYPE
        pr.ADDR = store.properties.ADDR
        pr.ARR = store.properties.ARR

        // pl.distance=stoÒre.dis
        pl.geometry = store.geometry
        pl._id = store._id

        pl.properties = pr
        featureCollection.features[i] = pl
        logger.info(i + ': ' + JSON.stringify(store.properties.DENOM) + ' ' + JSON.stringify(store.properties.NOM) + ' ' + JSON.stringify(store.properties.ARR))
        i = i + 1
      })
      return callback(null, featureCollection)
    }
  )
}

exports.neargames = function (req, res) {
  var swlat = Number(req.query.swlat)
  var swlng = Number(req.query.swlng)
  var nelat = Number(req.query.nelat)
  var nelng = Number(req.query.nelng)
  // var userlat=Number(req.query.userlat)
  // var userlng=Number(req.query.userlng)
  var kidage = 0
  if (req.query.age) {
    kidage = Number(req.query.age)
  }
  var free = 0
  if (req.query.free) {
    free = 'FREE_GAME'
  } else {
    free = ''
  }

  var query = makeQuery(swlat, swlng, nelat, nelng, kidage, free, 0, 0, null)
  gamesWithQuery(query,
    function (err, docs) {
      if (err) { return [] }
      if (req.query.json) {
        res.json(JSON.stringify(docs))
      } else {
        res.send(JSON.stringify(docs))
      }
    })
}

// GeoJSON Feature Collection
function FeatureCollection () {
  this.type = 'FeatureCollection'
  // this.features = PlaygroundL()
  this.features = []
}

function HourCollection () {
  this.type = 'HourCollection'
  this.periods = []
}

function Hours () {
  this.HOUR = []
  this.NAME = ''
}

function PlaygroundL () {
  // this.properties = new Array()
  this.properties = new PlaygroundProperties()
  // this.distance = 0
  this.geometry = ''
  this.type = 'Feature'
  this._id = ''
}

function RATPL () {
  // this.properties = new Array()
  this.properties = new RATPStopProperties()
  // this.distance = 0
  this.geometry = ''
  this.type = 'Feature'
  this._id = ''
}

function PlaygroundProperties () {
  this.DENOM = ''
  this.NOM = ''
  this.GLOBALMIN = 0
  this.GLOBALMAX = 18
  this.GAMES = []
  this.HORAIRE = ''
  this.ACCES = ''
  this.ARR = 0
  this.URL = ''
  this.TYPE = ''
  this.ADDR = ''
}

function RATPStopProperties () {
  this.STATION_ID = ''
  this.STATION_NOM = ''
  this.STATION_CITY = ''
  this.STATION_NETWORK = ''
  this.LIGNES = []
}

// function RATPLigne () {
//   this.LIGNE_CODE = ''
// }
